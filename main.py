# -*- coding: utf-8 -*-
import xlsxwriter
import requests
import data
from os import path
from bs4 import BeautifulSoup


PATH_DIR = f"{path.dirname(__file__)}\\output"
FILE_XLSX = f"{PATH_DIR}\\output.xlsx"
OUTPUT_IMG = f"{PATH_DIR}\\images"

HOME_URL = data.MAIN.HOME_URL
NAMES = data.MAIN.NAMES
URLS = data.MAIN.URLS
URLS_OBJECTS = data.MAIN.URLS_OBJECTS


class ParsingPage(object):
    def __init__(self, urls_names, urls_dict, urls_objects=None):
        self.names = urls_names
        self.urls = urls_dict
        if urls_objects is None:
            self.urls_obj = {}
        else:
            self.urls_obj = urls_objects
        self.data = {}

    def get_urls_objects(self):
        for name in self.names:
            self._get_urls_objects(name)

    def _get_urls_objects(self, name):
        if not self.urls_obj:
            r = requests.get(self.urls.get(name))
            for div in BeautifulSoup(r.content, 'html.parser').find_all(
                    "div", attrs={"class": "categoryContent"}):
                url_list = [
                    f"{HOME_URL}{a.get('href')}"
                    for a in div.find_all("a", attrs={"class": "title"})
                ]
                self.urls_obj.update({name: url_list})

    def get_data_objects(self):
        i = 0
        for name in self.names:
            self.data.update({name: []})
            list_ = self.data.get(name)
            for url in self.urls_obj.get(name):
                content = self._get_page(url)
                list_.append(self._parsing_page(content))
                # if i == 2:
                #     return
                # i += 1

    @staticmethod
    def _get_page(url):
        r = requests.get(url)
        return r.content

    def _parsing_page(self, content):
        soup = BeautifulSoup(content, 'html.parser')
        content_inner = soup.find(id='content-inner')
        title = str(content_inner.h1.string)
        product_content_first = content_inner.find(
            'div', attrs={'class': 'productContent'}
        )
        product_content_secondary = content_inner.find(
            'div', attrs={'class': 'productSecondary'}
        )
        desc, price_one, price_killo, img_name, img_url = \
            self._get_data_first(product_content_first)
        if product_content_secondary is not None:
            zoloto, sereb, platina, pallad, tantal, skandi, pli = \
                self._get_data_secondary(product_content_secondary)
        else:
            zoloto, sereb, platina, pallad, tantal, skandi, pli = \
                '', '', '', '', '', '', ''

        return [
            title, desc, price_one, price_killo, img_name, img_url,
            self._prepare_atrr_title(title),
            zoloto, sereb, platina, pallad, tantal, skandi, pli
        ]

    @staticmethod
    def _prepare_atrr_title(title_src):
        if 'Cкупка' in title_src or 'Скупка' in title_src:
            title = title_src.split(None, 1)[1]
        else:
            title = title_src

        return title

    @staticmethod
    def _get_data_first(content):
        price_one = ''
        price_killo = ''
        desc = str(content.p.string)
        for price in content.find_all('div', attrs={'class': 'price'}):
            if 'штуку' in price.string:
                price_one = price.string.split(' ')[-2]
            elif 'килограмм' in price.string:
                price_killo = price.string.split(' ')[-2]
        img = content.img['src']
        img_name = img.split('/')[-1]
        img_url = f"{HOME_URL}{img}"

        return (
            desc, price_one, price_killo, img_name, img_url
        )

    @staticmethod
    def _get_data_secondary(content):
        table = content.find(
            'table', attrs={'class': 'table'}
        ).tbody.find_all('tr')

        zoloto = table[0].find_all('td')[1].text
        sereb = table[1].find_all('td')[1].text
        platina = table[2].find_all('td')[1].text
        pallad = table[3].find_all('td')[1].text
        tantal = table[4].find_all('td')[1].text
        skandi = table[5].find_all('td')[1].text
        pli = table[6].find_all('td')[1].text

        return (
            zoloto, sereb, platina, pallad, tantal, skandi, pli
        )

    def write_to_xlsx(self):
        workbook = xlsxwriter.Workbook(FILE_XLSX.replace('\\', '/'))
        worksheet = workbook.add_worksheet()

        bold = workbook.add_format({'bold': 1})

        worksheet.set_column(1, 1, 15)
        worksheet.write('A1', 'Title', bold)
        worksheet.write('B1', 'Desc', bold)
        worksheet.write('C1', 'Category', bold)
        worksheet.write('D1', 'Price', bold)
        worksheet.write('E1', 'Price kg', bold)
        worksheet.write('F1', 'Img', bold)
        worksheet.write('G1', 'title_metal', bold)
        worksheet.write('H1', 'Zoloto', bold)
        worksheet.write('I1', 'Sereb', bold)
        worksheet.write('J1', 'Platina', bold)
        worksheet.write('K1', 'Pallad', bold)
        worksheet.write('L1', 'Tantal', bold)
        worksheet.write('M1', 'Skandi', bold)
        worksheet.write('N1', 'PlI', bold)

        data = self._prepare_data_on_write()

        row = 1

        for title, desc, category, price_one, price_killo, name_file, \
            img_url, title_metal, zoloto, sereb, \
                platina, pallad, tantal, skandi, pli in data:

            worksheet.write_string(row, 0, title)
            worksheet.write_string(row, 1, desc)
            worksheet.write_string(row, 2, category)
            worksheet.write_string(row, 3, price_one)
            worksheet.write_string(row, 4, price_killo)
            worksheet.write_string(row, 5, name_file)
            worksheet.write_string(row, 6, title_metal)
            worksheet.write_string(row, 7, zoloto)
            worksheet.write_string(row, 8, sereb)
            worksheet.write_string(row, 9, platina)
            worksheet.write_string(row, 10, pallad)
            worksheet.write_string(row, 11, tantal)
            worksheet.write_string(row, 12, skandi)
            worksheet.write_string(row, 13, pli)

            self._write_file_image(img_url, name_file)

            row += 1

        workbook.close()

    def _prepare_data_on_write(self):
        data = []

        i = 0
        for name in self.names:

            for title, desc, price_one, price_killo, \
                img_name, img_url, title_metal, zoloto, sereb, platina, \
                    pallad, tantal, skandi, pli in (self.data.get(name)):

                name_file = self._get_file_name(name, img_name)

                data.append(
                    [title, desc, name, price_one, price_killo,
                     name_file, img_url, title_metal, zoloto, sereb,
                     platina, pallad, tantal, skandi, pli]
                )
                # if i == 2:
                #     return data
                # i += 1
        return data

    @staticmethod
    def _get_file_name(name, img_name):
        if name == 'Конденсаторы':
            pre = 'capacitors'
        elif name == 'Радиолампы':
            pre = 'radio_tubes'
        elif name == 'Микросхемы':
            pre = 'chips'
        elif name == 'Переключатели':
            pre = 'switches'
        elif name == 'Разъемы':
            pre = 'connectors'
        elif name == 'Резисторы':
            pre = 'resistors'
        elif name == 'Реле':
            pre = 'relay'
        elif name == 'ЭВМ':
            pre = 'computer'
        elif name == 'Транзисторы':
            pre = 'transistors'
        else:
            pre = 'none'

        return f"{pre}_{img_name}"

    @staticmethod
    def _write_file_image(url, name):
        f = open(f"{OUTPUT_IMG}\\{name}".replace('\\', '/'), "wb")
        ufr = requests.get(url)
        f.write(ufr.content)
        f.close()


if __name__ == "__main__":
    app_parsing = ParsingPage(NAMES, URLS, URLS_OBJECTS)
    app_parsing.get_data_objects()
    app_parsing.write_to_xlsx()
    print(app_parsing.data)
